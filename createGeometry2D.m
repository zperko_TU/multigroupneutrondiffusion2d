function [model,booleanTable] = createGeometry2D(model,modelData,runData)
%createGeometry2D creates a 2D geometry for a PDE
% 1. INPUT:
%   - model: a PDEmodel object
%   - modelData: structure with model information
%   - runData: structure with information on the requested calculation
%       - checkGeometry: flag to plot geometry, edges and faces
%       - BCs: structure for boundary conditions, neumann and dirichlet
%       - IC: initial condition for time dependent problems
%       - runType: run type, can be 'fixedsource','eigenvalue','time'
% 2. OUTPUT
%   - model: the updated PDEModel object
%   - booleanTable: the Boolean table of the geometry.
% 
%% COPYRIGHT
% Zoltan Perko, TU Delft, 2020.10.24
%
%% FUNCTION DEFINITION
% Geometry description
% Rectangle [3,4, 4 x coord. followed by 4 y coord]
R1 = [3,4,-1.5,1.5,1.5,-1.5,1.5,1.5,-1.5,-1.5]';
% Rectangle defined as polygon, [2,n,n x coord. followed by n y coord]
R2 = [2,4,-1.5,1.5,1.5,-1.5,1,1, 1.5, 1.5]';
R3 = [2,4,-1.5,1.5,1.5,-1.5,-1,-1, -1.5, -1.5]';
% Circle, nRows must be the same as for the rectangle/other geometries
C1 = [1,0,0,0.6]';
C1 = [C1; zeros(numel(R1)-numel(C1),1);];
% Make geometry description matrix
gd = [R1,R2,R3,C1];
% Naming
ns = char('Rectangle1','Rectangle2','Rectangle3','Circle1');
ns = ns';
% Set formula for geometry
sf = 'Rectangle1+Rectangle2+Rectangle3+Circle1';
% Create geometry with 2 domains, returning the geometry matrix
% dl and Boolean table relating the original shapes to the minimal region
[geometry,booleanTable] = decsg(gd,sf,ns);

% Plot if requested
if runData.checkGeometry
    figure
    pdegplot(geometry,'EdgeLabels','on','FaceLabels','on');
end

% Set geometry
geometryFromEdges(model,geometry);

% Set BCs
if ~isempty(runData.BCs.dirichlet.edge)
    applyBoundaryCondition(model,'dirichlet','Edge',...
        runData.BCs.dirichlet.edge,'u',runData.BCs.dirichlet.u);
end
if ~isempty(runData.BCs.neumann.edge)
    applyBoundaryCondition(model,'neumann','Edge',...
        runData.BCs.neumann.edge,'q',runData.BCs.neumann.q,'g',runData.BCs.neumann.g);
end

if strcmpi(runData.runType,'time')
    for i=1:numel(runData.IC)
        setInitialConditions(model,runData.IC(i).u,'Face',runData.IC(i).Face);
    end
end

if numel(modelData.materialIDs)~=model.Geometry.NumFaces ...
        || numel(modelData.sourceIDs)~=model.Geometry.NumFaces
    warning('Material and/or fixed source assignment to 2D geometry faces inconsistent');
end

end