function model = createMesh2D(model,runData)

generateMesh(model,'Hmax',runData.hmax);
if runData.checkGeometry
    figure
    pdemesh(model);
    axis equal;
end

end