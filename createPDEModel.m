function [model,modelData,parameters,relPerturbations] = createPDEModel(runData,modelData,perturbations)
%createPDEModel creates the PDE model for the multigroup 2D diffusion eq.,
%including possible perturbations to the parameters

% Create PDEModel
model = createpde(runData.nGroups);

% Load test geometry
[model,~] = createGeometry2D(model,modelData,runData);

% Apply perturbations if passed
if nargin == 3
    [modelData,parameters,relPerturbations] = perturbProperties(modelData,perturbations);
end

end