function [XS,XSInfo] = loadXS(dataset)
%LoadXS loads a dataset of cross sections.
% 1. INPUT:
%   - dataset: flag for dataset, '2G' or 'C5G7'
% 2. OUTPUT: 
%   - XS: array of structures (1 for each material) with cross section
%   data in multigroup format, with entries Sigmat, Sigmaf, nuSigmaf,
%   nuDelay, energyPerFission, Chi, Sigmas, SigmasTot, Sigmac and D;
%   and nuclideID for an ID of the material.
%   - XSInfo: a structure with the extra XS information, with entries:
%       - nNuclides: number of materials with XS data
%       - nGroup: the number of energy groups
%       - iFirstThermalGroup: First group with upscatter
%       - nPrecursors: number of precursor groups
%       - scatterOrder: the order of scatter (for scatter matrices)
%       - energyBoundaries: the energy boundaries for the groups (J).
%       - vGroup: group velocities for time dependent problems (cm/s).
%       - referenceTemperature: Temperature for the XS data (K).
%       - precursors: precursor data, with delayedNeutronFractions and
%       decayConstants.
% 3. TODO:
%   - implement some proper interface for loading data from JEFF or ENDF
%   files
%
%% COPYRIGHT
% Zoltan Perko, TU Delft, 2020.10.24.
%
%% FUNCTION DEFINITION
switch(dataset)
    case 'C5G7'
        % C5G7 dataset
        XSInfo.referenceTemperature = 900; %K
        XSInfo.nNuclides = 2;
        XSInfo.nGroups = 7;
        XSInfo.nPrecursors = 6;
        XSInfo.scatterOrder = 0;
        % Group data
        XSInfo.iFirstThermalGroup = 1;
        XSInfo.energyBoundaries = [1.964e7,8.1873e6,1.2246e6,8.2298e4,...
            4.54e2,6.1601,0.18,9.9999997e-6]*1.602e-19;
        v = 100*sqrt(2/1.67492749804e-27*XSInfo.energyBoundaries);
        XSInfo.vGroup = 2./(1./v(1:end-1)+1./v(2:end))';
        % Precursor data, beta_i/beta_tot [nPrecursors]
        XSInfo.precursors.delayedNeutronFractions = [0.060,0.364,0.349,0.628,0.179,0.070];
        % Decay constants \lambda_i
        XSInfo.precursors.decayConstants =[0.0129,0.0311,0.134,0.331,1.26,3.21];
        
        % XS data
        XS(1).nuclideID = '1111';
        XS(1).Sigmat = [1.77949e-1,3.29805e-1,4.80388e-1,5.54367e-1,3.11801e-1,3.95168e-1,5.64406e-1];
        XS(1).Sigmaf = [7.21206e-3,8.19301e-4,6.45320e-3,1.85648e-2,1.78084e-2,8.30348e-2,2.16004e-1];
        XS(1).nuSigmaf = [2.005998429e-02,2.027302973e-03,1.570599176e-02,4.518301024e-02,4.334208392e-02,2.020900962e-01,5.257105352e-01];
        XS(1).nuDelay = [0.e0,0.e0,0.e0,0.e0,0.e0,0.e0,0.e0];
        XS(1).energyPerFission = [3.240722e-11,3.240722e-11,3.240722e-11,3.240722e-11,3.240722e-11,3.240722e-11,3.240722e-11];
        XS(1).Chi = [5.87910e-1,4.11760e-1,3.39060e-4,1.17610e-7,0.e0,0.e0,0.e0];
        XS(1).Sigmas(:,:,1) = ... %\Sigmas(g,g') = \Sigmas_{g'->g}
            [1.27537e-1 0.e0       0.e0       0.e0       0.e0       0.e0       0.e0      ;
            4.23780e-2 3.24456e-1 0.e0       0.e0       0.e0       0.e0       0.e0      ;
            9.43740e-6 1.63140e-3 4.50940e-1 0.e0       0.e0       0.e0       0.e0      ;
            5.51630e-9 3.14270e-9 2.67920e-3 4.52565e-1 1.25250e-4 0.e0       0.e0      ;
            0.e0       0.e0       0.e0       5.56640e-3 2.71401e-1 1.29680e-3 0.e0      ;
            0.e0       0.e0       0.e0       0.e0       1.02550e-2 2.65802e-1 8.54580e-3;
            0.e0       0.e0       0.e0       0.e0       1.00210e-8 1.68090e-2 2.73080e-1 ];
        XS(1).SigmasTot = sum(XS(1).Sigmas,1);
        XS(1).Sigmac = XS(1).Sigmat - XS(1).Sigmaf - XS(1).SigmasTot;
        XS(1).D = 1./(3*XS(1).Sigmat);
        
        XS(2).nuclideID = '7777';
        XS(2).Sigmat = [1.59206e-1,4.12970e-1,5.90310e-1,5.84350e-1,7.18000e-1,1.25445e+0,2.65038e+0];
        XS(2).Sigmaf = [0.e0,0.e0,0.e0,0.e0,0.e0,0.e0,0.e0];
        XS(2).nuSigmaf = [0.e0,0.e0,0.e0,0.e0,0.e0,0.e0,0.e0];
        XS(2).nuDelay = [0.e0,0.e0,0.e0,0.e0,0.e0,0.e0,0.e0];
        XS(2).energyPerFission = [0.e0,0.e0,0.e0,0.e0,0.e0,0.e0,0.e0];
        XS(2).Chi = [0.e0,0.e0,0.e0,0.e0,0.e0,0.e0,0.e0];
        XS(2).Sigmas(:,:,1) = ... %\Sigmas(g,g') = \Sigmas_{g'->g}
            [4.44777e-2 0.e0       0.e0       0.e0       0.e0       0.e0       0.e0      ;
            1.13400e-1 2.82334e-1 0.e0       0.e0       0.e0       0.e0       0.e0      ;
            7.23470e-4 1.29940e-1 3.45256e-1 0.e0       0.e0       0.e0       0.e0      ;
            3.74990e-6 6.23400e-4 2.24570e-1 9.10284e-2 7.14370e-5 0.e0       0.e0      ;
            5.31840e-8 4.80020e-5 1.69990e-2 4.15510e-1 1.39138e-1 2.21570e-3 0.e0      ;
            0.e0       7.44860e-6 2.64430e-3 6.37320e-2 5.11820e-1 6.99913e-1 1.32440e-1;
            0.e0       1.04550e-6 5.03440e-4 1.21390e-2 6.12290e-2 5.37320e-1 2.48070e-0 ];
        XS(2).SigmasTot = sum(XS(2).Sigmas,1);
        XS(2).Sigmac = XS(2).Sigmat - XS(2).Sigmaf - XS(2).SigmasTot;
        XS(2).D = 1./(3*XS(2).Sigmat);
        
    case 'G2'
        % 2 group dataset
        XSInfo.referenceTemperature = 900; %K
        XSInfo.nNuclides = 2;
        XSInfo.nGroups = 2;
        XSInfo.nPrecursors = 6;
        XSInfo.scatterOrder = 0;
        % Group data
        XSInfo.iFirstThermalGroup = 1;
        XSInfo.energyBoundaries = [1.964e7,0.18,9.9999997e-6]*1.602e-19;
        XSInfo.vGroup = sqrt(2/1.67492749804e-27*...
            (XSInfo.energyBoundaries(1:end-1)+XSInfo.energyBoundaries(2:end))/2)';
        v = 100*sqrt(2/1.67492749804e-27*XSInfo.energyBoundaries);
        XSInfo.vGroup = 2./(1./v(1:end-1)+1./v(2:end))';
        % Precursor data, beta_i/beta_tot [nPrecursors]
        XSInfo.precursors.delayedNeutronFractions = [0.060,0.364,0.349,0.628,0.179,0.070];
        % Decay constants \lambda_i
        XSInfo.precursors.decayConstants =[0.0129,0.0311,0.134,0.331,1.26,3.21];
        
        % XS data
        XS(1).nuclideID = '1111';
        XS(1).Sigmat = [0.04,0.139];
        XS(1).Sigmaf = [0.005,0.05];
        XS(1).nuSigmaf = [3*0.005, 2.5*0.05];
        XS(1).nuDelay = [0.e0,0.e0];
        XS(1).energyPerFission = [3.240722e-11,3.240722e-11];
        XS(1).Chi = [1,0];
        XS(1).Sigmas(:,:,1) = ... %\Sigmas(g,g') = \Sigmas_{g'->g}
            [0.0075, 0;
            0.02, 0.015];
        XS(1).SigmasTot = sum(XS(1).Sigmas,1);
        XS(1).Sigmac = XS(1).Sigmat - XS(1).Sigmaf - XS(1).SigmasTot;
        XS(1).D = 1./(3*XS(1).Sigmat);
        
        XS(2).nuclideID = '7777';
        XS(2).Sigmat = [0.08,0.439];
        XS(2).Sigmaf = [0.0,0.0];
        XS(2).nuSigmaf = [0.0, 0.0];
        XS(2).nuDelay = [0.e0,0.e0];
        XS(2).energyPerFission = [3.240722e-11,3.240722e-11];
        XS(2).Chi = [0,0];
        XS(2).Sigmas(:,:,1) = ... %\Sigmas(g,g') = \Sigmas_{g'->g}
            [0.0075, 0;
            0.02, 0.015];
        XS(2).SigmasTot = sum(XS(2).Sigmas,1);
        XS(2).Sigmac = XS(2).Sigmat - XS(2).Sigmaf - XS(2).SigmasTot;
        XS(2).D = 1./(3*XS(2).Sigmat);
end

end