function [model,results] = multiGroupNeutronDiffusion2D(model,modelData,runData)
%multiGroupNeutronDiffusion2D solves the multigroup neutron diffusion equation
%in a 2 dimensional geometry as a fixed source, eigenvalue of time
%dependent problem.
% 1. INPUT:
%   - model: a PDEModel object, containing geometry, BC and IC data
%   - modelData: a structure containing material and source data for the
%   problem
%   - runData: a structure containing all calculation details:
%       - runType: flag for type of problem to be solved, can be 'time',
%       'eigenvalue','fixedsource'.
%       - timePoints: The time values at which the solution is requested.
%       - checkGeometry: logical flag to plot geometry and faces/edges.
%       - plotSolution: logical flag to plot solution
% 2. OUTPUT:
%   - model: matlab PDEModel object with all details of the problem
%   - results: solution of the PDE
% 3. DESCRIPTION: Matlab's in-built PDE solver is used to discretize the
% problem in space using finite-elements.
% 4. TODO: 
%   - Implement delayed neutrons
%   - Implement proper input handling.
%
%% COPYRIGHT
% Zoltan Perko, TU Delft, 2020.10.23
%
%% FUNCTION DEFINITION
%
% Manipulate XS if adjoint calculation is requested
if runData.adjointFlag == true
    modelData.XS = setXSForAdjoint(modelData.XS);
end

% Create PDE coefficients for m*d^2u/dt^2+d*du/dt-\nabla*(c*\nabla u)+a*u=f
% or -\nabla*(c*\nabla u) + a*u = \lambda*d*u
% We have 1/v_g*\pdiff{\phi_g}{t} -
% \nabla\cdot\rb{D_g\nabla\phi_g}+\Sigma_{R_g}\phi_g = S_g
m = 0;
switch(lower(runData.runType))
    case 'time'
        if runData.adjointFlag == true
            d = -1./modelData.XSInfo.vGroup;
        else
            d = 1./modelData.XSInfo.vGroup;
        end
        a = @(location,state)(coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmat') - ...
            coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmas') - ...
            coeffunction(location,state,modelData.materialIDs,modelData.XS,'ChinuSigmaf'));
        f = @(location,state)externalSource(location,state,modelData.sourceIDs,modelData.fixedSource);
    case 'fixedsource'
        d = 0;
        a = @(location,state)(coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmat') - ...
            coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmas') - ...
            coeffunction(location,state,modelData.materialIDs,modelData.XS,'ChinuSigmaf'));
        f = @(location,state)externalSource(location,state,modelData.sourceIDs,modelData.fixedSource);
    case 'eigenvalue'
        d = @(location,state)coeffunction(location,state,modelData.materialIDs,modelData.XS,'ChinuSigmaf');
        a = @(location,state)(coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmat') - ...
            coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmas'));
        f = 0;
end
% Diffusion, use the 3N element column vector format to avoid ambiguities,
% c1;0;c1;c2;0;c2;...,cG;0;cG
c = @(location,state)coeffunction(location,state,modelData.materialIDs,modelData.XS,'D');
specifyCoefficients(model,'m',m,'d',d,'c',c,'a',a,'f',f);

% Generate mesh
[model] = createMesh2D(model,runData);

% Solve PDE
switch(lower(runData.runType))
    case 'time'
        results = solvepde(model,runData.timePoints);
        u = results.NodalSolution;
        plotPDESolution(model,u,runData)
    case 'eigenvalue'
        searchInterval = [0 1];
        while true
            results = solvepdeeig(model,searchInterval);
            u=results.Eigenvectors;
            if isempty(u)
                searchInterval = searchInterval(2)*[1 2];
            else
                for i=1:length(u(1,:))
                    if all(u(:,i)<=0)
                        u(:,i)=-u(:,i);
                    end
                end
                break
            end
            if searchInterval(2) > 1e4
                break
            end
        end
        plotPDESolution(model,u,runData)
    case 'fixedsource'
        results = solvepde(model);
        u=results.NodalSolution;
        plotPDESolution(model,u,runData)
end

% Re-set the cross sections if adjoint run was requested
if runData.adjointFlag == true
    modelData.XS = setXSForAdjoint(modelData.XS);
end
end


function coeff = coeffunction(location,~,materialIDs,XS,reaction)
nGroups = numel(XS(1).Sigmat);
subdomains = unique(location.subdomain);
switch(reaction)
    case 'D'
        % Using the 3N format for diagonal spatial dependence as c1;0;c1;c2;0;c2;...,cG;0;cG
        coeff = zeros(3*nGroups,length(location.subdomain));
        for g=1:nGroups
            for i=1:numel(subdomains)
                coeff([(g-1)*3+1,(g-1)*3+3],location.subdomain == subdomains(i)) = XS(materialIDs(subdomains(i))).(reaction)(g);
            end
        end
    case {'Sigmat','Sigmas','ChinuSigmaf'}
        % Using the N^2 format for non-symmetric scatter matrix
        coeff = zeros(nGroups*nGroups,length(location.subdomain));
        for g=1:nGroups
            for gTo=1:nGroups
                for i=1:numel(subdomains)
                    switch(reaction)
                        case 'Sigmat'
                            if g == gTo
                                coeff((g-1)*nGroups+gTo,location.subdomain == subdomains(i)) = XS(materialIDs(subdomains(i))).(reaction)(gTo);
                            end
                        case 'Sigmas'
                            coeff((g-1)*nGroups+gTo,location.subdomain == subdomains(i)) = XS(materialIDs(subdomains(i))).(reaction)(gTo,g);
                        case 'ChinuSigmaf'
                            coeff((g-1)*nGroups+gTo,location.subdomain == subdomains(i)) = XS(materialIDs(subdomains(i))).('Chi')(gTo).*...
                                XS(materialIDs(subdomains(i))).('nuSigmaf')(g);
                    end
                end
            end
        end
end

end

function f = externalSource(location,~,sourceIDs,fixedSource)
nGroups = numel(fixedSource(1,:));
f = zeros(nGroups,length(location.subdomain));
subdomains = unique(location.subdomain);
for i=1:numel(subdomains)
    for g=1:nGroups
        f(g,location.subdomain == subdomains(i)) = fixedSource(sourceIDs(subdomains(i)),g);
    end
end

end

function XS = setXSForAdjoint(XS)
    for materialID=1:numel(XS)
        ChiTemp = XS(materialID).Chi;
        XS(materialID).Chi = XS(materialID).nuSigmaf;
        XS(materialID).nuSigmaf = ChiTemp;
        for scatOrder = 1:length(XS(materialID).Sigmas(1,1,:))
            XS(materialID).Sigmas(:,:,scatOrder) = XS(materialID).Sigmas(:,:,scatOrder)';
        end
    end
end