function [model,results] = oneGroupNeutronDiffusion2D(model,modelData,runData)
%oneGroupNeutronDiffusion2D solves the 1 group neutron diffusion equation
%in a 2 dimensional geometry as a fixed source, eigenvalue of time
%dependent problem.
% 1. INPUT:
%   - model: a PDEModel object, containing geometry, BC and IC data
%   - modelData: a structure containing material and source data for the
%   problem
%   - runData: a structure containing all calculation details:
%       - runType: flag for type of problem to be solved, can be 'time',
%       'eigenvalue','fixedsource'.
%       - group: group number to use for the cross-sections
%       - timePoints: The time values at which the solution is requested.
%       - checkGeometry: logical flag to plot geometry and faces/edges.
%       - plotSolution: logical flag to plot solution
% 2. OUTPUT:
%   - model: matlab PDEModel object with all details of the problem
%   - results: solution of the PDE
% 3. DESCRIPTION: Matlab's in-built PDE solver is used to discretize the
% problem in space using finite-elements.
% 4. TODO: 
%   - Implement delayed neutrons
%   - Implement proper input handling.
%
%% COPYRIGHT
% Zoltan Perko, TU Delft, 2020.10.22
%
%% FUNCTION DEFINITION
%
% Create PDE coefficients for m*d^2u/dt^2+d*du/dt-\nabla*(c*\nabla u)+a*u=f
% or -\nabla*(c*\nabla u) + a*u = \lambda*d*u
% We have: 
% In forward model:  1/v_g*\pdiff{\phi_g}{t} -
% \nabla\cdot\rb{D_g\nabla\phi_g}+\Sigma_{R_g}\phi_g = S_g
% In adjoint model: -1/v_g*\pdiff{\phi_g^{\dagger}}{t} -
% \nabla\cdot\rb{D_g\nabla\phi_g^{\dagger}}+\Sigma_{R_g}\phi_g^{\dagger} =
% S_g^{\dagger}
m = 0;
switch(lower(runData.runType))
    case 'time'
        if runData.adjointFlag == true
            d = -1/modelData.XSInfo.vGroup(runData.group);
        else
            d = 1/modelData.XSInfo.vGroup(runData.group);
        end
        a = @(location,state)(coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmat',runData.group) - ...
            coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmas',runData.group) - ...
            coeffunction(location,state,modelData.materialIDs,modelData.XS,'nuSigmaf',runData.group));
        f = @(location,state)externalSource(location,state,modelData.sourceIDs,modelData.fixedSource,runData.group);
    case 'fixedsource'
        d = 0;
        a = @(location,state)(coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmat',runData.group) - ...
            coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmas',runData.group) - ...
            coeffunction(location,state,modelData.materialIDs,modelData.XS,'nuSigmaf',runData.group));
        f = @(location,state)externalSource(location,state,modelData.sourceIDs,modelData.fixedSource,runData.group);
    case 'eigenvalue'
        d = @(location,state)coeffunction(location,state,modelData.materialIDs,modelData.XS,'nuSigmaf',runData.group);
        a = @(location,state)(coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmat',runData.group) - ...
            coeffunction(location,state,modelData.materialIDs,modelData.XS,'Sigmas',runData.group));
        f = 0;
end

c = @(location,state)coeffunction(location,state,modelData.materialIDs,modelData.XS,'D',runData.group);
specifyCoefficients(model,'m',m,'d',d,'c',c,'a',a,'f',f);

% Generate mesh
[model] = createMesh2D(model,runData);

% Solve PDE
switch(lower(runData.runType))
    case 'time'
        results = solvepde(model,runData.timePoints);
        u = results.NodalSolution;
        plotPDESolution(model,u,runData)
    case 'eigenvalue'
        searchInterval = [0 1];
        while true
            results = solvepdeeig(model,searchInterval);
            u=results.Eigenvectors;
            if isempty(u)
                searchInterval = searchInterval(2)*[1 2];
            else
                for i=1:length(u(1,:))
                    if all(u(:,i)<=0)
                        u(:,i)=-u(:,i);
                    end
                end
                break
            end
            if searchInterval(2) > 1e4
                break
            end
        end
        plotPDESolution(model,u,runData)
    case 'fixedsource'
        results = solvepde(model);
        u=results.NodalSolution;
        plotPDESolution(model,u,runData)
end

end


function coeff = coeffunction(location,~,materialIDs,XS,reaction,group)
    coeff = zeros(1,length(location.subdomain));
    subdomains = unique(location.subdomain);
    switch(reaction)
        case 'Sigmas'
            for i=1:numel(subdomains)
                coeff(1,location.subdomain == subdomains(i)) = XS(materialIDs(subdomains(i))).(reaction)(group,group);
            end
        otherwise
            for i=1:numel(subdomains)
                coeff(1,location.subdomain == subdomains(i)) = XS(materialIDs(subdomains(i))).(reaction)(group);
            end
    end

end

function f = externalSource(location,~,sourceIDs,fixedSource,group)
    f = zeros(1,length(location.subdomain));
    subdomains = unique(location.subdomain);
    for i=1:numel(subdomains)
        f(1,location.subdomain == subdomains(i)) = fixedSource(sourceIDs(subdomains(i)),group);
    end
end