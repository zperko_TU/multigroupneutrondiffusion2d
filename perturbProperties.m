function [modelData,parameters,relPerturbations] = perturbProperties(modelData,perturbations)
%perturbProperties applies the perturbations to the modelData
% 1. INPUT:
%   - modelData: structure with the properties of the model
%   - perturbations: cell array with 1 perturbation in each entry, with
%       - matID: entry in XS
%       - reaction: reaction to perturb
%       - group: group to perturb
%       - pert: perturbation, as *(1+perturbation)
% 2. OUTPUT:
%   - modelData: updated structure with perturbed properties
%   - parameters: the perturbed parameters [cell of strings]
%   - relPerturbations: relative perturbation of properties [array]
%
%% COPYRIGHT
% Zoltan Perko, TU Delft, 2020.10.23
%
%% FUNCTION DEFINITION
parameters = cell(1,numel(perturbations));
relPerturbations = zeros(1,numel(perturbations));
for i=1:numel(perturbations)
    switch (perturbations{i}.type)
        case 'XS'
            switch perturbations{i}.reaction
                case 'Sigmas'
                    % Adjust total and scattertot XS if scatter XS is perturbed
                    modelData = addRelPerturbation(modelData,perturbations{i},'Sigmat');
                    modelData = addRelPerturbation(modelData,perturbations{i},'SigmasTot');
                    [modelData,relPerturbations(1,i)] = perturbRel(modelData,perturbations{i},'Sigmas');
                case 'SigmasTot'
                    % Adjust total and scatter XS if scattertot is perturbed
                    modelData = addRelPerturbation(modelData,perturbations{i},'Sigmat');
                    modelData = perturbRel(modelData,perturbations{i},'Sigmas');
                    [modelData,relPerturbations(1,i)] = perturbRel(modelData,perturbations{i},'SigmasTot');
                case 'Sigmac'
                    % Adjust total XS if capture XS is perturbed
                    modelData = addRelPerturbation(modelData,perturbations{i},'Sigmat');
                    [modelData,relPerturbations(1,i)] = perturbRel(modelData,perturbations{i},'Sigmac');
                case 'Sigmaf'
                    % Adjust total XS and nuSigmaf if fission XS is perturbed
                    modelData = addRelPerturbation(modelData,perturbations{i},'Sigmat');
                    modelData = perturbRel(modelData,perturbations{i},'nuSigmaf');
                    [modelData,relPerturbations(1,i)] = perturbRel(modelData,perturbations{i},'Sigmaf');
                case 'Sigmat'
                    for j={'Sigmaf','nuSigmaf','Sigmac','SigmasTot','Sigmas'}
                        modelData = perturbRel(modelData,perturbations{i},j{1});
                    end
                    [modelData,relPerturbations(1,i)] = perturbRel(modelData,perturbations{i},'Sigmat');
                case {'nuSigmaf','energyPerFission','D','nuDelay'}
                    % nuSigmaf perturbation assumed to be nu perturbation
                    [modelData,relPerturbations(1,i)] = perturbRel(modelData,perturbations{i},perturbations{i}.reaction);
                case 'Chi' % Needs special case to handle constraint, these are unconstrained perturbations now
                    [modelData,relPerturbations(1,i)] = perturbRel(modelData,perturbations{i},perturbations{i}.reaction);
                otherwise
                    warning('createPDEModel:perturbProperties:UnsupportedXS',...
                        'Only Sigmas, SigmasTot, Sigmac, Sigmaf, Sigmat, nuSigmaf, D, nuDelay, Chi and energyPerFission supported');
            end
        parameters{1,i} = strcat('matID_',num2str(perturbations{i}.matID),'_reaction_',perturbations{i}.reaction,'_G_',replace(num2str(perturbations{i}.group),'  ','_'));
            
        case 'source'
            modelData.fixedSource(perturbations{i}.sourceID,perturbations{i}.group) = ...
                modelData.fixedSource(perturbations{i}.sourceID,perturbations{i}.group)*(1+perturbations{i}.pert);
            relPerturbations(1,i) = perturbations{i}.pert;
            parameters{1,i} = strcat('sourceID_',num2str(perturbations{i}.sourceID),'_G_',num2str(perturbations{i}.group));
            
        case 'vGroup'
            modelData.XSInfo.vGroup(perturbations{i}.group) = modelData.XSInfo.vGroup(perturbations{i}.group)*(1+perturbations{i}.pert);
            relPerturbations(1,i) = perturbations{i}.pert;
            parameters{1,i} = strcat('vGroup_G_',num2str(perturbations{i}.group));
        otherwise
            warning('createPDEModel:UnsupportedPerturbationType','Only XS, source and velocity perturbations supported');
    end
end

end

% Applies the relative perturbation in perturbation to reaction
function [modelData,relPert] = perturbRel(modelData,perturbation,reaction)
switch reaction
    case 'Sigmas'
        if strcmpi(perturbation.reaction,'Sigmat') || strcmpi(perturbation.reaction,'SigmasTot')
            % If total or scatter XS is perturbed, we need to adjust the whole
            % scatter matrix accordingly
            modelData.(perturbation.type)(perturbation.matID).(reaction)(:,perturbation.group(end)) = ...
                modelData.(perturbation.type)(perturbation.matID).(reaction)(:,perturbation.group(end))*(1+perturbation.pert);
            relPert = perturbation.pert;
        elseif strcmpi(perturbation.reaction,'Sigmas')
            % If the scatter matrix itself is perturbed, we only need to
            % adjust the required entry in the scatter matrix.
            modelData.(perturbation.type)(perturbation.matID).(reaction)(perturbation.group(1),perturbation.group(2)) = ...
                modelData.(perturbation.type)(perturbation.matID).(reaction)(perturbation.group(1),perturbation.group(2))*(1+perturbation.pert);
            relPert = perturbation.pert;
        end
    otherwise
        if strcmpi(perturbation.reaction,'Sigmas')
            modelData.(perturbation.type)(perturbation.matID).(reaction)(perturbation.group(1),perturbation.group(end)) = ...
                modelData.(perturbation.type)(perturbation.matID).(reaction)(perturbation.group(1),perturbation.group(end))*(1+perturbation.pert);
            relPert = perturbation.pert;
        else
            modelData.(perturbation.type)(perturbation.matID).(reaction)(perturbation.group) = ...
                modelData.(perturbation.type)(perturbation.matID).(reaction)(perturbation.group)*(1+perturbation.pert);
            relPert = perturbation.pert;
        end
end
end

% Adds the relative XS perturbation in perturbation to the reaction XS of
% reaction
function modelData = addRelPerturbation(modelData,perturbation,reaction)
if strcmpi(perturbation.reaction,'Sigmas')
    DeltaSigma = modelData.(perturbation.type)(perturbation.matID).(perturbation.reaction)(perturbation.group(1),perturbation.group(2))*...
        perturbation.pert;
else
    DeltaSigma = modelData.(perturbation.type)(perturbation.matID).(perturbation.reaction)(perturbation.group)*...
        perturbation.pert;
end
modelData.(perturbation.type)(perturbation.matID).(reaction)(perturbation.group(end)) = ...
    modelData.(perturbation.type)(perturbation.matID).(reaction)(perturbation.group(end)) + ...
    DeltaSigma;
end