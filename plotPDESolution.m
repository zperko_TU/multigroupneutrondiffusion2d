function plotPDESolution(model,u,runData)
%plotPDESolution plots the solution of the PDE if requested, making a video
%for timedependent problems.
% 1. INPUT:
%   - model: a PDEModel class object
%   - u: the solution of the PDE
%   - runData: a structure with the data for the problem
%
%% COPYRIGHT
% Zoltan Perko, TU Delft, 2020.10.23.
%
%% FUNCTION DEFINITION

if runData.plotSolution
    switch(runData.runType)
        case 'time'
            figure;
            figure('units','normalized','outerposition',[0 0 1 1]);
            axis tight manual;
            if runData.oneGroup
                solMin = min(u(:));
                solMax = max(u(:));    
                for i=1:numel(runData.timePoints)
                    pdeplot(model,'XYData',u(:,i),'ZData',u(:,i));
                    title(['Time: ',num2str(runData.timePoints(i)),' s']);
                    caxis([solMin solMax]);
                    set(gca,'zlim',[0 solMax]);
                    drawnow
                    if i==1
                        gif(runData.filename,'DelayTime',0.2,'frame',gcf);
                        gif;
                    else
                        gif;
                    end
                end
            else
                [nU,nGroups,nTime] = size(u);
                solMin = min(reshape(permute(u,[1 3 2]),[nU*nTime,nGroups]),[],1);
                solMax = max(reshape(permute(u,[1 3 2]),[nU*nTime,nGroups]),[],1);
                for i=1:numel(runData.timePoints)
                    for g=1:nGroups
                        subplot(ceil(sqrt(nGroups)),ceil(sqrt(nGroups)),g);
                        pdeplot(model,'XYData',u(:,g,i),'ZData',u(:,g,i));
                        title(['Group ',num2str(g)]);                       
                        caxis([solMin(g) solMax(g)]);
                        set(gca,'zlim',[0 solMax(g)]);
                    end
                    sgtitle(['Time: ',num2str(runData.timePoints(i)),' s']);
                    drawnow
                    if i==1
                        gif(runData.filename,'DelayTime',0.2,'frame',gcf); 
                    else
                        gif;
                    end
                end
            end
        case {'eigenvalue','fixedsource'}
            figure
            if runData.nGroups == 1
                pdeplot(model,'XYData',u(:,1),'ZData',u(:,1));
            else
                nGroups = numel(u(1,:));
                assert(nGroups==runData.nGroups,'Group number mismatch between runData and solution.');
                for g=1:nGroups
                    subplot(ceil(sqrt(nGroups)),ceil(sqrt(nGroups)),g);
                    pdeplot(model,'XYData',u(:,g),'ZData',u(:,g));
                    title(['Group: ',num2str(g)]);
                end
            end
        otherwise
            warning('plotPDESolution:runTypeError','Unrecognized runType');
    end
end

end