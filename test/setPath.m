function setPath(depthFlag)
%setPath adds the current folder, potentially together with its
%subdirectories or parent folder to the Matlab path
% 1. INPUT:
%   - depthFlag: flag indication if only current folder (0), current and
%   parent folder (<0), or current and all sub-directories (>0) should be
%   added to the path
%
%% COPYRIGHT
% Zoltan Perko, TU Delft, 2020.10.23.
%
%% FUNCTION DEFINITION
%
folderPath=fileparts(mfilename('fullpath'));
if depthFlag<0
    addpath(folderPath);
    parentFolder = fileparts(folderPath);
    addpath(parentFolder);
elseif depthFlag>0
    addpath(genpath(folderPath));
else
    addpath(folderPath);
end        
end